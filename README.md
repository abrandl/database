# Database Team

This repository is used to track database work that covers all of GitLab.com.

This repository is **not** meant for reporting bugs or performance problems,
those should instead be reported in the [GitLab Community
Edition](https://gitlab.com/gitlab-org/gitlab-ce/issues) issue tracker.

Small change - please ignore.